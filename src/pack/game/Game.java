package pack.game;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
public class Game extends JPanel{
	private static final long serialVersionUID = 1L;
	Ball ball = new Ball(this);
	Slider slider = new Slider(this);
	
	public Game() {
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				slider.keyReleased(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {
				slider.keyPressed(e);
			}
		});
		setFocusable(true);
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		ball.paint(g2d);
		slider.paint(g2d);
	}
	
	private void move() {
		ball.move();
		slider.move();
	}
	
	public void gameOver() {
		JOptionPane.showMessageDialog(this, "Game Over. Your score is "+ball.score, "Game Over", JOptionPane.OK_OPTION);
		System.exit(ABORT);
	}
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Initialising play area...");
		JFrame frame = new JFrame("Racquet Ball");
		Game game = new Game();
		frame.add(game);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		while (true) {
			game.move();
			game.repaint();
			Thread.sleep(10);
		}
	}
}
